const webcamElement = document.getElementById('webcam');
const canvasElement = document.getElementById('canvas');
const snapSoundElement = document.getElementById('snapSound');
const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);


function  takePhoto(){
    var picture = webcam.snap();
    localStorage.setItem('picture',picture);
    console.log(picture)
    // document.getElementById('file_upload').src = picture;
    // console.log(picture.replace(/^data:image\/(png|jpg);base64,/, ""))
    // console.log(dataURItoBlob(picture))
}
function stopCamera(){
    webcam.stop();
}
 function fliCamera(){
    webcam.flip();
     webcam.start();
}
function initializeCamera(){
    webcam.start()
        .then(result =>{
            // console.log("webcam started");
        })
        .catch(err => {
            console.log(err);
        });
}

function dataURItoBlob(dataURI) {
    const splitDataURI = dataURI.split(',');
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i);

    return new Blob([ia], { type: mimeString });
    // var binary = atob(dataURI.split(',')[1]);
    // var array = [];
    // for(var i = 0; i < binary.length; i++) {
    //     array.push(binary.charCodeAt(i));
    // }
    // return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}
