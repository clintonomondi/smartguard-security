<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Smart Guard</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="/asset/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/asset/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="/asset/css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="/images/logo.jpg" />

    <link href="/landing/css/plugins.css" rel="stylesheet" >
    <!-- Custom CSS -->
    <link href="/landing/css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="/asset/vendors/jquery-toast-plugin/jquery.toast.min.css">
    <link href="/css/toggle.css" rel="stylesheet">
    <link href="/css/mystyle.css" rel="stylesheet">

    <script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
    <link rel="stylesheet" href="/asset/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="/asset/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous" />

</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="{{ mix('js/app.js') }}"></script>



<script src="/asset/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="/asset/vendors/jquery-steps/jquery.steps.min.js"></script>
<script src="/asset/vendors/jquery-validation/jquery.validate.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="/asset/js/off-canvas.js"></script>
<script src="/asset/js/hoverable-collapse.js"></script>
<script src="/asset/js/template.js"></script>
<script src="/asset/js/settings.js"></script>
<script src="/asset/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="/asset/js/wizard.js"></script>
<!-- End custom js for this page-->

{{--<script src="/js/wizard.js"></script>--}}
<script src="/js/buttonloader.js"></script>
<script src="/asset/vendors/jquery-toast-plugin/jquery.toast.min.js"></script>
<!-- Custom js for this page-->
<script src="/asset/js/toastDemo.js"></script>
{{--<script src="/asset/js/desktop-notification.js"></script>--}}
<!-- End custom js for this page-->
<script src="/asset/vendors/chart.js/Chart.min.js"></script>

<script src="/asset/js/chart.js"></script>
<script src="/asset/vendors/typeahead.js/typeahead.bundle.min.js"></script>
<script src="/asset/vendors/select2/select2.min.js"></script>
<script src="/asset/js/typeahead.js"></script>
<script src="/asset/js/select2.js"></script>






<!--jquery js -->
{{--<script src="/landing/js/jquery-min.js"></script>--}}
<script src="/landing/js/popper.min.js"></script>
<!--jquery js -->
{{--<script src="/landing/js/bootstrap.min.js"></script>--}}
<!--jquery js -->
<script src="/landing/js/plugins.js"></script>
<!--Owl js -->
<script src="/landing/js/owl.js"></script>
<!--Fontawesome js -->
<script src="/landing/js/fontawesome.js"></script>
<!-- MagnificPopup JS -->
<script src="/landing/js/jquery.magnific-popup.min.js"></script>
<!-- Meanmenu JS -->
<script src="/landing/js/meanmenu.js"></script>
<!-- Count-to JS -->
<script src="/landing/js/count-to.js"></script>
<!-- jQuery Appear JS -->
<script src="/landing/js/jquery.appear.js"></script>
<!--jquery js -->
<script src="/landing/js/custom.js"></script>

<script src="/loader/center-loader.js"></script>
<script src="/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="/js/style.js"></script>

</body>
</html>
