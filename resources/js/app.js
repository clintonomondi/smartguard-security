import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './View/App'
import Index from './Pages/index'
import Login from './Pages/login'
import Select from './Pages/select'
import Register from './Pages/register'
import Home from './Pages/home'
import Clients from './Clients/clients'
import Add_Clinets from './Clients/add_client'
import Edit_client from './Clients/edit_client'
import Branches from './Branches/branches'
import Add_Branches from './Branches/add_branch'
import Edit_Branches from './Branches/edit_branch'
import Guards from './Guards/guards'
import Input from './Data/input'
import Department from './Department/department'
import GuardReport from './Guards/reports'
import Forget from './Pages/forget'
import Confirm from './Pages/confirm'
import ChangePass from './Pages/changepassword'
import BranchReport from './Branches/reports'
import CompanyReport from './Report/company_report'
import Profile from './Pages/profile'
import Company_Users from './User/company_users'
import Branch_Users from './User/brach_users'
import Client_guards from './Guards/client_guards'
import Terms from './Pages/terms'
import Blacklist from './Blacklist/client_blacklist'
import Epass from './Epass/epass'
import Location from './Location/location'






const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
            meta: { hideNavigation: true }
        },
        {
            path: '/locations',
            name: 'locations',
            component: Location,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/terms',
            name: 'terms',
            component: Terms,
            meta: { hideNavigation: true }
        },
        {
            path: '/select',
            name: 'select',
            component: Select,
            meta: { hideNavigation: true }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients
        },
        {
            path: '/clients/add',
            name: 'addclient',
            component: Add_Clinets
        },
        {
            path: '/client/details/:id',
            name: 'get_cleint_details',
            component: Edit_client
        },
        {
            path: '/branches',
            name: 'branches',
            component: Branches
        },
        {
            path: '/branches/add/:id',
            name: 'add_branches',
            component: Add_Branches
        },
        // { path: '/404', component: NotFound },
        // { path: '*', redirect: '/404' },
        //
        {
            path: '/branch/edit/:id',
            name: 'edit_branch',
            component: Edit_Branches
        },
        {
            path: '/guards',
            name: 'guards',
            component: Guards
        },
        {
            path: '/input',
            name: 'input',
            component: Input
        },
        {
            path: '/department',
            name: 'department',
            component: Department
        },
        {
            path: '/guard/reports',
            name: 'guardreport',
            component: GuardReport
        },
        {
            path: '/password/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/password/confirm',
            name: 'confirm',
            component: Confirm,
            meta: { hideNavigation: true }
        },
        {
            path: '/password/change',
            name: 'changepassword',
            component: ChangePass,
            meta: { hideNavigation: true }
        },
        {
            path: '/btranch/reports',
            name: 'branch_report',
            component: BranchReport,
        },
        {
            path: '/company/reports',
            name: 'company_report',
            component: CompanyReport,
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
        },
        {
            path: '/company/users',
            name: 'company_users',
            component: Company_Users,
        },
        {
            path: '/branch/users',
            name: 'branch_users',
            component: Branch_Users,
        },
        {
            path: '/branch/guards',
            name: 'client_guards',
            component: Client_guards,
        },
        {
            path: '/branch/blacklist',
            name: 'blacklist',
            component: Blacklist,
        },
        {
            path: '/client/epass',
            name: 'epass',
            component: Epass,
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

